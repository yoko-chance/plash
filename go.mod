module gitlab.com/yoko-chance/plash

go 1.22.1

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	gitlab.com/yoko-chance/slurp v0.2.3
	golang.org/x/term v0.0.0-20201117132131-f5c789dd3221
)

require golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
